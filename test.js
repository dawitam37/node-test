const {expect} = require('chai');
const {GenericContainer, Wait} = require('testcontainers');
const axios = require('axios');
const {resolve} = require("path");

describe('Node.js Server Test', function () {
    this.timeout(60000);

    let container;

    before(async () => {
        try {
            const projectDirectory = resolve(__dirname, '.');
            container = await new GenericContainer('node:16')
                .withBindMounts([
                    {
                        source: projectDirectory,
                        target: "/app",
                        mode: "ro",
                    },
                ])
                .withCommand([
                    "sh",
                    "-c",
                    "cd /app && npm start",
                ])
                .withExposedPorts(3000)
                .withWaitStrategy(Wait.forLogMessage('Server is running at http://localhost:3000/'))
                .start();

        } catch (e) {
            console.log(e)
        }
    });

    after(async () => {
        if (container) {
            await container.stop();
        }
    });

    it('should return "Hello, world!" from the server', async () => {
        const serverHost = container.getHost();
        const serverPort = container.getMappedPort(3000);
        const url = `http://${serverHost}:${serverPort}/`;

        try {
            const response = await axios.get(url);
            expect(response.status).to.equal(200);
            expect(response.data).to.equal('Hello, world!');
        } catch (error) {
            throw error;
        }
    });
});
